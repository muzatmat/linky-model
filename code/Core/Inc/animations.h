/*
 * animations.h
 *
 *  Created on: May 25, 2020
 *      Author: Matej
 */

#ifndef INC_ANIMATIONS_H_
#define INC_ANIMATIONS_H_

/**
 * Blikající animace, spíše pro účely debugování.
 */
uint8_t animation_blinky[] = { 0xFF, 0, 0, // GREEN
		0xFF, 0, 0, // GREEN
		0xFF, 0, 0, // GREEN
		0xFF, 0, 0, // GREEN
		0xFF, 0, 0, // GREEN
		0xFF, 0, 0, // GREEN
		0, 0, 0xFF, // BLUE
		0, 0, 0xFF, // BLUE
		0, 0, 0xFF, // BLUE
		0, 0, 0xFF, // BLUE
		0, 0, 0xFF, // BLUE
		0, 0, 0xFF, // BLUE
		0xFF, 0xFF, 0, // YELLOW
		0xFF, 0xFF, 0, // YELLOW
		0xFF, 0xFF, 0, // YELLOW
		0xFF, 0xFF, 0, // YELLOW
		0xFF, 0xFF, 0, // YELLOW
		0xFF, 0xFF, 0, // YELLOW
		0, 0xff, 0xff, // MAGENTA
		0, 0xff, 0xff, // MAGENTA
		0, 0xff, 0xff, // MAGENTA
		0, 0xff, 0xff, // MAGENTA
		0, 0xff, 0xff, // MAGENTA
		0, 0xff, 0xff, // MAGENTA
		0xFF, 0, 0xFF, // CYAN
		0xFF, 0, 0xFF, // CYAN
		0xFF, 0, 0xFF, // CYAN
		0xFF, 0, 0xFF, // CYAN
		0xFF, 0, 0xFF, // CYAN
		0xFF, 0, 0xFF, // CYAN
		0, 0xff, 0, // RED
		0, 0xff, 0, // RED
		0, 0xff, 0, // RED
		0, 0xff, 0 // RED
		};

/**
 * Animace s pruhy purpurové a azurové, které cestují zdola nahorů.
 * Tato animace je v ukázkovém videu.
 */
uint8_t animation_rain[] = {

/**********/
/* MAGENTA */
/*********/

/* FRAME 1 */
0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 2 */

		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 3 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 4 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 5 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 6 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 7 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 8 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 9 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 10 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 11 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 110, 110, // MAGENTA color
		0, 0, 0, // BLANK

		/* FRAME 12 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 110, 110, // MAGENTA color

		/**********/
		/* CYAN */
		/*********/

		/* FRAME 1 */
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 2 */

		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 3 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 4 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 5 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 6 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 7 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 8 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 9 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 10 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK

		/* FRAME 11 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		110, 0, 110, // CYAN color
		0, 0, 0, // BLANK

		/* FRAME 12 */
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		0, 0, 0, // BLANK
		110, 0, 110 // CYAN color

		};

#endif /* INC_ANIMATIONS_H_ */
