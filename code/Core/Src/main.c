/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "animations.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define HIGH_DUTY  38 /**< Hodnota střídy pro odeslání HIGH bitu pomocí 800kHz PWM */
#define LOW_DUTY 19	  /**< Hodnota střídy pro odeslání LOW bitu pomocí 800kHz PWM */
#define PIXEL_BYTE_WIDTH 3 /**< Počet bytů jedné LED (původních dat, barev) */
#define BYTE_BIT_WIDTH 8 /**< Počet bitů v jednom bytu */
#define PIXEL_BIT_WIDTH (PIXEL_BYTE_WIDTH * BYTE_BIT_WIDTH) /**< Počet bitů jednoho pixelu */
#define CONTROL_LED_PRESENT 1 /**< Příznak použití "kontrolní" LED, která nesvítí (např. znehodnocena pájením) */
#define CONTROL_BIT_WIDTH (CONTROL_LED_PRESENT * PIXEL_BIT_WIDTH) /**< Počet bitů "kontrolní" LED */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/**
 * @brief Buffer s hodnotami střídy, zdroj dat pro DMA.
 *
 * Délka vyplývá z přítomnosti "kontrolní" LED, počtu LED.
 * 1 byte je přidán navíc (nulový), aby přenos skončil ihned po skončení dat.
 */
uint8_t strip_buffer[CONTROL_BIT_WIDTH + LEDS_PER_STRIP * PIXEL_BIT_WIDTH + 1];
uint8_t *animation = animation_blinky; /**< Právě přehrávaná animace. Pole bytů barev v pořadí GRB */
size_t animation_length = sizeof(animation_blinky) / (PIXEL_BYTE_WIDTH * TOTAL_LEDS); /**< Snímková délka animace. */
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */
void strips_init(void);
void update_leds(void);
void flash_all_strips(size_t frame);
void load_buffer(uint8_t *data);
void change_animation(uint8_t *animation_source, size_t animation_length,
		uint8_t fps);
void load_strip(size_t frame_offset, size_t strip_offset);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */
	strips_init();
	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */
	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_TIM3_Init();
	MX_TIM2_Init();
	/* USER CODE BEGIN 2 */

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	change_animation(animation_rain,
			sizeof(animation_rain) / (LEDS_PER_STRIP * PIXEL_BYTE_WIDTH), 8);
//  change_animation(animation_blinky, 1, 2);
	while (1) {

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48;
	RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI48;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief TIM2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM2_Init(void) {

	/* USER CODE BEGIN TIM2_Init 0 */

	/* USER CODE END TIM2_Init 0 */

	LL_TIM_InitTypeDef TIM_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);

	/* TIM2 interrupt Init */
	NVIC_SetPriority(TIM2_IRQn, 0);
	NVIC_EnableIRQ(TIM2_IRQn);

	/* USER CODE BEGIN TIM2_Init 1 */

	/* USER CODE END TIM2_Init 1 */
	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 11999999;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM2, &TIM_InitStruct);
	LL_TIM_DisableARRPreload(TIM2);
	LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM2);
	/* USER CODE BEGIN TIM2_Init 2 */
	LL_TIM_EnableIT_UPDATE(TIM2);
	LL_TIM_EnableCounter(TIM2);
	/* USER CODE END TIM2_Init 2 */

}

/**
 * @brief TIM3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM3_Init(void) {

	/* USER CODE BEGIN TIM3_Init 0 */

	/* USER CODE END TIM3_Init 0 */

	LL_TIM_InitTypeDef TIM_InitStruct = { 0 };
	LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = { 0 };

	LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);

	/* TIM3 DMA Init */

	/* TIM3_CH4_UP Init */
	LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_3,
			LL_DMA_DIRECTION_MEMORY_TO_PERIPH);

	LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PRIORITY_LOW);

	LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MODE_NORMAL);

	LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PERIPH_NOINCREMENT);

	LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MEMORY_INCREMENT);

	LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_PDATAALIGN_HALFWORD);

	LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MDATAALIGN_BYTE);

	/* USER CODE BEGIN TIM3_Init 1 */

	/* USER CODE END TIM3_Init 1 */
	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 59;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM3, &TIM_InitStruct);
	LL_TIM_DisableARRPreload(TIM3);
	LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_OC_EnablePreload(TIM3, LL_TIM_CHANNEL_CH4);
	TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM1;
	TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.CompareValue = 0;
	TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH;
	LL_TIM_OC_Init(TIM3, LL_TIM_CHANNEL_CH4, &TIM_OC_InitStruct);
	LL_TIM_OC_DisableFast(TIM3, LL_TIM_CHANNEL_CH4);
	LL_TIM_SetTriggerOutput(TIM3, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM3);
	/* USER CODE BEGIN TIM3_Init 2 */
	LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH4);
	LL_TIM_EnableCounter(TIM3);
	/* USER CODE END TIM3_Init 2 */
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
	/**TIM3 GPIO Configuration
	 PB1   ------> TIM3_CH4
	 */
	GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
	GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/** 
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void) {

	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA1_Channel2_3_IRQn interrupt configuration */
	NVIC_SetPriority(DMA1_Channel2_3_IRQn, 2);
	NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(Debug_LED_GPIO_Port, Debug_LED_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin : Debug_LED_Pin */
	GPIO_InitStruct.Pin = Debug_LED_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(Debug_LED_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/**
 * @brief Inicializace pásků.
 *
 * Inicializuje poslední byte bufferu nulový,
 * jelikož jsem měl předtím problémy, že přenos neskončí okamžitě.
 * Také inicializuje začátek bufferu střídou pro nulový bit,
 * aby kontrolní LED nesvítila (tedy pokud je kontrolní LED používána).
 */
void strips_init() {
	strip_buffer[sizeof(strip_buffer) - 1] = 0; /* Aby přenos po skončení dat nepokračoval */
	memset(strip_buffer, LOW_DUTY, CONTROL_BIT_WIDTH);
}

/**
 * @brief Převede bit na hodnotu střídy pro odeslání
 *        pomocí 800kHz PWM.
 *
 * Převáděna je pouze hodnota LSB (bit nejvíce vpravo).
 *
 * @param bit byte jehož LSB bude převedeno.
 * @retval Hodnotu střídy LOW pokud je LSB nulový, hodnotu střídy HIGH jinak.
 *
 */
uint8_t convert_bit_to_duty(uint8_t bit) {
	return (bit << 7) ? HIGH_DUTY : LOW_DUTY;
}

/**
 * @brief Nahraje snímek do bufferu.
 *
 * Na vstupu bere ukazatel na pole bytů barev v pořadí G, R, B
 * prostě za sebou. Tato data převede do bufferu, kde každý
 * byte odpovídá jednomu bitu původních dat.
 *
 * @param data ukazatel na pole bytů barev
 */
void load_buffer(uint8_t *data) {
	for (size_t i = 0; i < LEDS_PER_STRIP; i++) {
		for (size_t j = 0; j < PIXEL_BYTE_WIDTH; j++) {
			for (size_t k = 0; k < BYTE_BIT_WIDTH; k++) {
				strip_buffer[CONTROL_BIT_WIDTH + i * PIXEL_BIT_WIDTH
						+ j * BYTE_BIT_WIDTH + k] = convert_bit_to_duty(
						data[i * PIXEL_BYTE_WIDTH + j] >> k);
			}
		}
	}
}

/**
 * @brief Odešle aktuální buffer na datový kanál pomocí PWM.
 */
void update_leds() {
	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_3);

	LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_3, (uint32_t) &strip_buffer[0],
			(uint32_t) &TIM3->CCR4, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);

	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, sizeof(strip_buffer));
	LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_3, LL_DMA_MODE_NORMAL);

	LL_DMA_DisableIT_HT(DMA1, LL_DMA_CHANNEL_3);
	LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_3);

	LL_TIM_EnableDMAReq_UPDATE(TIM3);

	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
}

/**
 * @brief Problikne po jednom všechny pásky.
 *
 * Zatím nepoužívám 2 a více odlišných pásků,
 * v odevzdávané verzi jsou všechny pásky napojené
 * na jeden datový kanál. Tato funkce tedy ještě
 * bude upravována.
 *
 * @param frame počet již přehraných snímků (index snímku, který chceme přehrát).
 */
void flash_all_strips(size_t frame) {
	for (size_t s = 0; s < TOTAL_STRIPS; s++) {
		load_strip(frame, s);
		update_leds();
	}
}

/**
 *  @brief Nahraje do bufferu daný pásek z daného snímku.
 *
 *  @param frame index snímku, který chceme nahrát do bufferu.
 *  @param strip index pásku, počítáno v rámci snímku.s
 */
void load_strip(size_t frame, size_t strip) {
	load_buffer(
			animation + frame * TOTAL_LEDS * PIXEL_BYTE_WIDTH
					+ strip * LEDS_PER_STRIP * PIXEL_BYTE_WIDTH);
}

/**
 *  @brief Pozastaví animaci.
 */
void pause_animation() {
	LL_TIM_DisableIT_UPDATE(TIM2);
}

/**
 *  @brief Spustí animaci.
 */
void begin_animation() {
	LL_TIM_EnableIT_UPDATE(TIM2);
}

/**
 *  @brief Pozastaví animaci, změní ji a opět spustí.
 *
 *  @param source ukazatel na pole bytů barev animace, pořadí GRB.
 *  @param length počet snímků animace.
 *  @param fps počet snímků za vteřinu, které chceme přehrávat.
 */
void change_animation(uint8_t *source, size_t length, uint8_t fps) {
	pause_animation();

	animation = source;
	animation_length = length;
	LL_TIM_SetAutoReload(TIM2, (48000000 / fps) - 1);

	begin_animation();
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
