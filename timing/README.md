# Časování komunikace s LED WS2812B
***Důležité! Časování svépomocí pravěpodobně nedopadne, možná až po odevzdání
projektu – viz sekce Aktuální stav na této stránce dole***


## Přístup
Původně jsem na to chtěl jít zbytečně složitě (viz. níže),
pomocí počítání cyklů TIM2 jsem měřil velmi krátké časové úseky.
To byl samozřejmě problém, protože každé volání nějaké funkce
trochu měnilo počet instrukcí a ovlivňovalo přesnost.

Na internetu jsem se však inspiroval, že podobnou věc lze řešit
pomocí PWM a DMA (viz. protokol z úlohy *X-7*). To mi přijde jako
elegantní řešení a zkusím se do toho pustit.

Jednotlivé bity se posílají pomocí následujících kódů:

![](dsh_pic.png "Kódy pro komunikaci s WS281B")


<table>
<caption><em>Přehled mezních hodnot dle datasheetu</em></caption>
<tbody>
<tr>
<td><strong><span style="text-decoration:underline;">Symbol</span></strong></td>
<td><span style="text-decoration:underline;"><strong>Parameter</strong></span></td>
<td align="right"><span style="text-decoration:underline;"><strong>Min</strong></span></td>
<td align="right"><span style="text-decoration:underline;"><strong>Typical</strong></span></td>
<td align="right"><span style="text-decoration:underline;"><strong>Max</strong></span></td>
<td align="right"><span style="text-decoration:underline;"><strong>Units</strong></span></td>
</tr>
<tr>
<td>T0H</td>
<td>0 code, high voltage time</td>
<td align="right">250</td>
<td align="right">400</td>
<td align="right">550</td>
<td align="right">ns</td>
</tr>
<tr>
<td>T1H</td>
<td>1 code, high voltage time</td>
<td align="right">650</td>
<td align="right">800</td>
<td align="right">950</td>
<td align="right">ns</td>
</tr>
<tr>
<td>T0L</td>
<td>0 code, low voltage time</td>
<td align="right">700</td>
<td align="right">850</td>
<td align="right">1000</td>
<td align="right">ns</td>
</tr>
<tr>
<td>T1L</td>
<td>1 code, low voltage time</td>
<td align="right">300</td>
<td align="right">450</td>
<td align="right">600</td>
<td align="right">ns</td>
</tr>
<tr>
<td>RES</td>
<td>reset, low voltage time</td>
<td align="right">50 000</td>
<td></td>
<td></td>
<td align="right">ns</td>
</tr>
</tbody>
</table>

Každý bit trvá 1.25 μs, liší se pouze dobou trvání, viz tabulka výše.
Tato pravidelnost se hodí - můžu mít PWM s pevnou frekvencí a měnit pouze střídu podle toho, jestli posílám bitovou 1, nebo 0.

Podobně jako v úloze *X-7*, budu mít připravené pole s hodnotami střídy,
pomocí DMA jej přesouvat do registru TIMx->CCR4.
Použiji jeden čítač, o frekvenci 800 kHz (potřebuji totiž periodu 1,25 μs).
Na něm bude i PWM, jehož střídu (TIMx->CCR4) budu měnit.
Čítač bude nastaven tak, aby přetékal po každé periodě (přeteče 800000krát za sekundu). Přetečení dá DMA instrukci, aby přesunula z předpřipraveného pole následující hodnotu střídy do registru TIMx->CCR4.

Použiji TIM3, zdroj nastavím na Internal clock, bez Prescaleru,
Clock period nastavím na 59 (abych dosáhl "dělení" šedesáti, ale zároveň
mohl přesněji nastavit střídu).

Hodnoty střídy jsou v následující tabulce:

|   Kód   | Střída      |   Hodnota CCR4    |
| ------  | ----------  |   :----------:    |
| *0 code*|  32 %       |       19          |
| *1 code*|  64 %       |       38          |

Mám hotovo, úspěšně komunikuji s chytrými LED (zatím s jednou).
Mám 48bytové pole, každý byte reprezentuje hodnotu registru pro daný bit.
Každá polovina pole (24 bytů) reprezentuje jeden pixel. 
Každá osmice bytů reprezentuje původní byte jedné barvy, 
avšak místo jedniček a nul jsou použity přímo hodnoty střídy (CCR4)
(proto to je osmice bytů v poli, a ne byte).

Zatím jsem pole vygeneroval staticky, v jedné polovině mám jen 0 (přehnaně dlouhý RESET kód, nevadí, nemá žádné maximum).

Ve druhé polovině mám červenou barvu. Skutečně to funguje,
připojil jsem k mikrokontroléru třetici pixelů, po naprogramování
první z nich svítí červeně.

Dlouho jsem měl potíže to rozchodit a nemohl najít chybu,
zjistil jsem, že hodnota střídy se nemění a zůstává původní z inicializace.

Problém byl v tom, že jsem zapomněl na řádek

```
LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
```
V inicializaci TIM3 jsem měl pouze řádek

```
LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH4);
```

Který je velmi podobný, to mě nejspíše zmátlo.

Jinak jsem s řešením velmi spokojen,
nejvíce se mi na něm líbí jeho efektivita.
Samotný kód programu je totiž téměř prázdný – 
v něm nyní akorát generuji původní stav pole, 
což jsou vlastně pouze testovací data.
Jinak se celá "logika" odehrává pouze v konfiguraci DMA v několika málo řádcích (z nichž jsem však jeden zapomněl a pěkně mě to potrápilo).

### Dvoufázové plnění zdrojového pole – *"double buffer"*
Jak jsem již zmínil, mé testovací zdrojové pole (buffer) má 48 bytů,
čili místo celkem na 2 pixely. To se mi trochu hodilo při testování (reset v jedné polovině), ale také pomocí toho chci načítat data.

DMA totiž může vyvolat přerušení při načtení poloviny dat (HT - *half transfer complete*) a při úplném načtení dat (TC - *transfer complete*).
Když mi přijde událost HT, vím že mám čas na přepsání první poloviny
bufferu dalšími daty. Když mi přijde událost TC, vím, že DMA už je opět na začátku pole bufferu, a můžu tedy přepsat data v druhé polovině pole, novými daty.
Takto mohu celý proces opakovat dokud je potřeba. 

Po odeslání všech pixelů (1 pásek) nesmím zapomenout odeslat RESET.
RESET o době trvání jednoho pixelu (240 μs) je zbytečně dlouhý, stačí
50 μs.
To zrealizuji tak, že vyplním pouze 40 (50 μs, možná dám o něco vyšší)
bytů v bufferu nulami, a přenastavím DMA:
- adresu inicializuji opět na buffer (DMA se pak zresetuje a pojede znovu od začátku)
- mód nebude CIRCULAR ale NORMAL (nechceme opakovat)
- data length bude pouze 40 (či to dané číslo o něco málo vyšší)

50 μs je dlouhá doba, takže mezitím co se DMA stará o "zasílání RESET kódu",
hlavní program může kopírovat další pixel (další pás, další frame?) do nevyužívané poloviny bufferu.

Po skončení zasílání RESET kódu dostanu opět TC událost přerušení,
v té zjistím, že jsem právě resetoval (pomocí flagu)
a přenastavím DMA opět do běžného režimu.

## Z čeho vychází limity v datasheetu?

- **T0H** puls musí trvat dostatečně dlouho, aby byl zaznamenán
- **T0H** puls nesmí překročit určitou mez, aby se z něj nestal kód pro binární 1
- **T1H** puls musí trvat dostatečně dlouho, aby nebyl brán jako binární 0
- **TxL** pulsy musí trvat dostatečně dlouho, aby správně oddělily bity
- **TxL** pulsy nesmí překročit určitou mez, aby nebyly brány jako RESET kód 

## Původní přístup – komplikovaný, neudržitelný
Zkusil jsem měřit velmi časové úseky pomocí registru CNT čítače TIM2 předděleného 6 (časový úsek o délce 125 ns, osmina mikrosekundy). 
Vytvořil jsem funkci eighth_micro_delay(times) a pomocí ní pak zkusil
naimplementovat komunikaci podle datasheetu.
Komunikace ale nefunguje ani pro 1 LED, chování je spíše náhodné - přesnost
tedy pravděpodobně není dostatečná.
