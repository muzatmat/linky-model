# Popis struktury souborů s animacemi
Binární soubor s animací bude začínat informací o snímkové frekvenci (*FPS*),
zbytek budou informace o jednotlivých světelných bodech v pořadí RGBA.

## Pořadí světelných bodů
Světelné budou v souboru uloženy shora dolů, po pásech zleva doprava
(viz následující obrázek, kde každé číslo reprezentuje jeden světelný bod).

![Pořadí světelných bodů v souboru](poradi.png "Pořadí světelných bodů v souboru")

Jednotlivé snímky animace budou v souboru uloženy chronologicky, od prvního do posledního.


## Detailní tabulka
*n* je počet světelných bodů na jeden vertikální pás,
v mém případě *n = 34*.

*f* je počet snímků v animaci, libovolně vysoký (max. dle velikosti úložiště – SD karty).

Co se týče světelných bodů (pixelů), každý řádek tabulky respektuje pravidla předchozího řádku a přidává pravidlo další (viz obrázek výše)

| Čísla bytů        | Obsah                         | Přípustné hodnoty |
| ----------------- | ----------------------------- | ----------------- |
| byte 0            | FPS (počet snímků za sekundu) | 1 až 50           |
| byte 1 až byte 3  | horní pixel levého panelu (pořadí GRB) | 0 až 255 |
| byte 4 až byte *3n* | zbylé pixely levého panelu (shora dolů) | 0 až 255 |
| byte *3n + 1* až byte *5 × 3n* | zbylé pixely všech panelů (zleva doprava) | 0 až 255 
| byte *(5 × 3n + 1)* až byte *(f × 5 × 3n)* | zbylé pixely všech snímků (první až *f*-tý) | 0 až 255

## Ukázkové animace
Na animacích nestrávím příliš času, jelikož bych chtěl projekt dokončit.
Následující animace zatím nejsou všechny hotové, jsou to jen nápady 
(zatím trávím čas na důležitějších částech projektu).

### Blikání (blink)
Takový hello world projekt, 2 snímky za sekundu,
Každý lichý snímek všechny body svítí bíle, každý sudý snímek žádný bod nesvítí.

### Barevný test
Test, zda-li funguje míchání barev.
Odspoda po šesti pixelech svítí na každém pásku zelená, modrá, žlutá, purpurová, azurová.
Na horních čtyřech diodách (přesah nad budovou) svítí červená.

### Česká vlajka
Obdoba české vlajky na skutečné světelné fasádě.
Statický obrázek připomínající českou vlajku.

### Přechody barev
Na všech pásech je vždy stejná barva.
Na celé ploše se plynule (50 FPS) mění barvy v následujícím pořadí:
ČERVENÁ => ORANŽOVÁ => ŽLUTÁ => ZELENÁ => MODRÁ => FIALOVÁ => ČERVENÁ => ...
