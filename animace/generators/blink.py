BRIGHTNESS = 100

f = open("../mcu/blink", "wb")
data = [2] # FPS

white_pixel = [0xff, 0xff, 0xff, BRIGHTNESS]
blank_pixel = [0, 0, 0, 0]

animation = white_pixel + blank_pixel
data.extend(animation) # connect animation with the header

f.write(bytearray(data))
f.close()