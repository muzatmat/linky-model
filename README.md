# Funkční model světelné fasády FEL ČVUT
Jako semestrální práci do předmětu LPE
jsem se rozhodl udělat funkční model
světelné fasády budovy FEL ČVUT v Dejvicích
(Linky FEL ČVUT).
Tento objekt se mi totiž líbí a jeho ztvárnění v podobě modelu by mohlo být zajímavé.

Půjde o zajímavou úlohu i kvůli následujícím
aspektům:
- Potřeba dodávat velký proud
- Načasování animace
- Uchování aktuální animace v operační paměti
- Uchování více animací
- Forma (ovládání, model samotné fasády...)

Kvůli nedostatku zkušeností jsem se snažil vybrat si jednoduché a zajímavé zadání,
k němuž jsem dovymyslel několik nepovinných části, 
které mohu zrealizovat, když vše zvládnu včas a dobře (viz níže).

## Zadání
Student vytvoří model světelné fasády budovy FEL ČVUT v Dejvicích.

### Povinné požadavky
- Model bude přehrávat animace
  - Každá animace může mít jinou snímkovou frekvenci
  - Animace je definovaná polem snímků + metadata (informace o snímkové frekvenci, 1 až 50 FPS)
  - Snímek je definován jako pole bodů
  - ~~Pixel je definován jako pole hodnot RGBW, každá 8 bitů (i skutečná světelná fasáda používá model RGBW)~~
  - Pixel je definován jako pole hodnot RGB, každá 8 bitů (skutečná světelná fasáda používá model RGBW, ale LED WS2812B používají model RGB)
- Model bude mít alespoň 4 vestavěné ukázkové animace,
mezi nimiž bude možné přepínat pomocí tlačítka

### Nepovinné požadavky, nápady
- Model funguje jako access point WiFi
  - Po připojení k němu je možné zobrazit webové rozhraní, které poskytuje
  - Přes rozhraní je možné ovládat fasádu (přepínat mezi vestavěnými animacemi)
  - Přes rozhraní je možné odeslat animaci k přehrání (soubor formátu JSON s podobnou či stejnou strukturou jako definuje [oficiální API](https://linky.fel.cvut.cz/Api#scroll_2))
- Model je schopen se připojit k internetu
  - Přes [oficiální API](https://linky.fel.cvut.cz/Api#scroll_3_2)
  může zjišťovat aktuální stav světelné fasády v Dejvicích
  a replikovat jej na fasádě modelu (na modelu by pak bylo to samé, jako na opravdové fasádě, s drobným zpožděním)

<div style="page-break-after: always;"></div>

## Poznámky ohledně řešení
Model budu realizovat pomocí 5 pásků "chytrých" LED WS2812B,
které mám k dispozici. Mám dva metrové kusy, na každém je 144 diod. Jejich funkčnost mám otestovanou, včetně stříhání a pájení.

Skutečná fasáda se skládá z 5 vertikálních pásů. Na každém je 204 světelných bodů. Ty jsou fyzicky rozdělené do panelů po 12 světelných bodech.

Já samozřejmě nemůžu udělat věrnou kopii se stejným rozlišením,
musím zvolit vhodné měřítko – aby rozlišení nebylo příliš malé, ale také aby model nebyl příliš velký (jsem limitovaný hustotou LED na jednotlivých páscích
– 144 LED/m).

Rozhodl jsem se, že pro přehlednost budu vycházet z počtů
světelných bodů na skutečné fasádě. Chci, aby počet světelných bodů na skutečné fasádě byl násobkem počtu LED na mojí fasádě.
V potaz beru 3 možnosti:
- 3 světelné body na 1 LED (varianta A)
- 6 světelných bodů na 1 LED (varianta B)
- 12 světelných bodů na 1 LED (varianta C)

Tři varianty popisuji v následující tabulce. 
Pro odhad používám spotřebu 50 mA / dioda, kterou uvádějí články na internetu týkající se WS2812B. 
Uvádím i proud na jeden vertikální pás, kdybych se rozhodl
nesvítit všemi pásy naráz, ale rychle mezi nimi blikal a spoléhal na setrvačnost lidského oka.

| Varianta | LED na pás | LED celkem | Proud na pás | Proud celkem | Výška   |
| :------: | :--------: | :--------: | :----------: | :----------: | :---:   |
| A        |    51      |     255    |      2550 mA |   12750 mA   | ~~734~~ 354 mm |
| B        |    34      |     170    |      1700 mA |    8500 mA   | ~~490~~ 236 mm |
| C        |    17      |      85    |       850 mA |    4250 mA   | ~~245~~ 118 mm |

~~Z hlediska výšky a spotřeby vidím, že jediná rozumná varianta pro mě je varianta C.~~
Původní výpočet byl špatný, počítal jsem tedy s variantou C, která by byla lepší
z hlediska spotřeby a výšky. Když jsem ale výšku přepočítal správně,
zjistil jsem, že model ve variantě C by byl příliš malý.
Budu si tedy muset poradit s velikostí proudu pro variantu B.

Animace chci v zařízení mít uložené na SD kartě, buď ve formátu JSON podle [oficiálního API](https://linky.fel.cvut.cz/Api#scroll_2), nebo lépe v souboru s předem definovanou strukturou, kde budou jednotlivé barevné informace bodů za sebou jako byty 
(aby nemusel mikrokontrolér vždy při načítání animace do operační paměti zpracovávat nějaký
textový soubor). Modul pro čtení z SD karty musím ještě pořídit.

Případná síťová vylepšení budu řešit pomocí ESP8266 modulu, který mám k dispozici.

Hotové pásky připevním na kartonový model naší fakulty, 
který mám slíbený od známého z fakulty architektury.
Nebude to věrný model, ale bude rozměrově odpovídat páskům,
a připomínat naši fakultu alespoň tvarem. Cokoliv, na čem někdo z fakulty architektury
nechá byť jen pár hodin práce bude rozhodně lepší, než kdybych výsledek já připevnil na krabici od bot.
Mikrokontrolér a ostatní periferie plánuji umístit dovnitř modelu.

<div style="page-break-after: always;"></div>
