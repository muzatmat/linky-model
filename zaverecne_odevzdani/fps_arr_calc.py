CLOCK_FREQ = 48000000

for fps in range(50):
    fps += 1
    arr =  CLOCK_FREQ // fps
    actual_fps = CLOCK_FREQ / arr
    difference = actual_fps - fps
    if difference:
        print("For wanted FPS {} the ARR is {} and actual FPS is {}. They differ by {}."\
            .format(fps, arr, actual_fps, difference))
        # print("{},{},{}".format(fps, arr, actual_fps))